# README

This project will allow you to take copied-and-pasted data from tickets 
or emails and convert them into CSV files for use in Excel or Google Sheets
for nicely formatted data.  

This is helpful to the value of `top` or `htop` when shared without formatting.

### Setup

- Clone this project
- Make the `convert` file an executable

```bash
chmod u+x convert
```
- Copy and paste your data into a `.txt` file (ex: `data.txt`)
- Run the converter:

```bash
./convert data.txt
```

- This will create a `data.csv` file that you can then import into Excel or Google Sheets